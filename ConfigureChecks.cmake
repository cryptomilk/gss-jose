include(CheckIncludeFile)
include(CheckSymbolExists)
include(CheckFunctionExists)
include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckStructHasMember)
include(CheckPrototypeDefinition)
include(TestBigEndian)

set(NEGOEX_PACKAGE ${PROJECT_NAME})
set(NEGOEX_VERSION ${PROJECT_VERSION})

function(COMPILER_DUMPVERSION _OUTPUT_VERSION)
    # Remove whitespaces from the argument.
    # This is needed for CC="ccache gcc" cmake ..
    string(REPLACE " " "" _C_COMPILER_ARG "${CMAKE_C_COMPILER_ARG1}")

    execute_process(
        COMMAND
            ${CMAKE_C_COMPILER} ${_C_COMPILER_ARG} -dumpversion
        OUTPUT_VARIABLE _COMPILER_VERSION
    )

    string(REGEX REPLACE "([0-9])\\.([0-9])(\\.[0-9])?" "\\1\\2"
           _COMPILER_VERSION "${_COMPILER_VERSION}")

    set(${_OUTPUT_VERSION} ${_COMPILER_VERSION} PARENT_SCOPE)
endfunction()

if(CMAKE_COMPILER_IS_GNUCC AND NOT MINGW AND NOT OS2)
    compiler_dumpversion(GNUCC_VERSION)
    if (NOT GNUCC_VERSION EQUAL 34)
        set(CMAKE_REQUIRED_FLAGS "-fvisibility=hidden")
        check_c_source_compiles(
"void __attribute__((visibility(\"default\"))) test() {}
int main(void){ return 0; }
" WITH_VISIBILITY_HIDDEN)
        unset(CMAKE_REQUIRED_FLAGS)
    endif (NOT GNUCC_VERSION EQUAL 34)
endif(CMAKE_COMPILER_IS_GNUCC AND NOT MINGW AND NOT OS2)

###########################################################
# For detecting attributes we need to treat warnings as
# errors
set(CMAKE_REQUIRED_FLAGS "-Werror")

check_c_source_compiles("
void test_constructor_attribute(void) __attribute__ ((constructor));

void test_constructor_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_CONSTRUCTOR_ATTRIBUTE)

check_c_source_compiles("
void test_destructor_attribute(void) __attribute__ ((destructor));

void test_destructor_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_DESTRUCTOR_ATTRIBUTE)

check_c_source_compiles("
#pragma init (test_constructor)
void test_constructor(void);

void test_constructor(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_PRAGMA_INIT)

check_c_source_compiles("
#pragma fini (test_destructor)
void test_destructor(void);

void test_destructor(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_PRAGMA_FINI)

check_c_source_compiles("
#define FALL_THROUGH __attribute__((fallthrough))

int main(void) {
    int i = 2;

    switch (i) {
    case 0:
        FALL_THROUGH;
    case 1:
        break;
    default:
        break;
    }

    return 0;
}" HAVE_FALLTHROUGH_ATTRIBUTE)

check_c_source_compiles("
__thread int tls;

int main(void) {
    return 0;
}" HAVE_GCC_THREAD_LOCAL_STORAGE)

check_c_source_compiles("
void log_fn(const char *format, ...) __attribute__ ((format (printf, 1, 2)));

int main(void) {
    return 0;
}" HAVE_FUNCTION_ATTRIBUTE_FORMAT)

check_c_source_compiles("
void test_address_sanitizer_attribute(void) __attribute__((no_sanitize_address));

void test_address_sanitizer_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_ADDRESS_SANITIZER_ATTRIBUTE)

# Stop treating wanrings as errors
unset(CMAKE_REQUIRED_FLAGS)
###########################################################

# ENDIAN
if (NOT WIN32)
    test_big_endian(WORDS_BIGENDIAN)
endif (NOT WIN32)
