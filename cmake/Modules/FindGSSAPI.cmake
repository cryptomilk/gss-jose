# - Try to find GSSAPI
# Once done this will define
#
#  GSSAPI_FOUND - system has GSSAPI
#  GSSAPI_INCLUDE_DIR - the GSSAPI include directory
#  GSSAPI_LIBRARY - Link these to use GSSAPI
#  GSSAPI_DEFINITIONS - Compiler switches required for using GSSAPI
#
#=============================================================================
#  Copyright (c) 2022 Andreas Schneider <asn@cryptomilk.org>
#
#  Distributed under the OSI-approved BSD License (the "License");
#  see accompanying file Copyright.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even the
#  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the License for more information.
#=============================================================================
#

find_package(PkgConfig)
if (PKG_CONFIG_FOUND)
  pkg_check_modules(PKG_GSSAPI mit-krb5-gssapi)
endif (PKG_CONFIG_FOUND)

set(GSSAPI_VERSION ${PKG_GSSAPI_VERSION})
set(GSSAPI_DEFINITIONS ${PKG_GSSAPI_CFLAGS_OTHER})

find_path(GSSAPI_INCLUDE_DIR
    NAMES
        gssapi/gssapi.h
    PATHS
        ${_GSSAPI_INCLUDEDIR}
)

find_library(GSSAPI_LIBRARY
    NAMES
        gssapi_krb5
    PATHS
        ${_GSSAPI_LIBDIR}
)

if (GSSAPI_LIBRARY)
    list(APPEND GSSAPI_LIBRARIES ${GSSAPI_LIBRARY})
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GSSAPI
                                  FOUND_VAR GSSAPI_FOUND
                                  REQUIRED_VARS GSSAPI_INCLUDE_DIR GSSAPI_LIBRARY
                                  VERSION_VAR GSSAPI_VERSION)

# show the GSSAPI_INCLUDE_DIR and GSSAPI_LIBRARIES variables only in the advanced view
mark_as_advanced(GSSAPI_INCLUDE_DIR GSSAPI_LIBRARY)
