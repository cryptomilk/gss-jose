project(gss-jose-ssp C)

find_package(GSSAPI 1.18 REQUIRED)

set(gss-jose_SOURCES
    gss-jose.c
    gss-spi.c
    gss_metadata.c
    gss_sec_ctx.c
    gss_status.c)

add_library(gss-jose MODULE ${gss-jose_SOURCES})

target_include_directories(gss-jose
    PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
           $<INSTALL_INTERFACE:include>
    PRIVATE ${CMAKE_BINARY_DIR} ${GSSAPI_INCLUDE_DIR})

target_compile_options(gss-jose
                       PRIVATE ${DEFAULT_C_COMPILE_FLAGS} -D_GNU_SOURCE)

target_link_libraries(gss-jose PRIVATE defaults ${GSSAPI_LIBRARY})

set_target_properties(gss-jose
                      PROPERTIES
                          VERSION ${LIBRARY_VERSION}
                          SOVERSION ${LIBRARY_SOVERSION}
                          PREFIX "")

set(GSS_JOSE_LOCATION
    "${CMAKE_CURRENT_BINARY_DIR}/gss-jose${CMAKE_SHARED_LIBRARY_SUFFIX}"
    PARENT_SCOPE)

if (WITH_VISIBILITY_HIDDEN)
    set(EXPORTS_PATH ${CMAKE_CURRENT_SOURCE_DIR}/gss-jose.exports)

    target_link_libraries(gss-jose PRIVATE "-Wl,--version-script,\"${EXPORTS_PATH}\"")
endif()

install(TARGETS gss-jose
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/gss-jose)
