/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdlib.h>

#include "gss-jose.h"

OM_uint32 gss_jose_init_sec_context(OM_uint32 *minor_status,
                                    gss_cred_id_t claimant_cred_handle,
                                    gss_ctx_id_t *context_handle,
                                    gss_name_t target_name,
                                    gss_OID mech_type,
                                    OM_uint32 req_flags,
                                    OM_uint32 time_req,
                                    gss_channel_bindings_t input_chan_bindings,
                                    gss_buffer_t input_token,
                                    gss_OID *actual_mech,
                                    gss_buffer_t output_token,
                                    OM_uint32 *ret_flags,
                                    OM_uint32 *time_rec)
{
    struct gss_jose_ctx *jctx = NULL;

    *minor_status = 0;

    if (context_handle == NULL) {
        return GSS_S_NO_CONTEXT;
    }

    jctx = (struct gss_jose_ctx *)*context_handle;
    if (jctx == NULL) {
        jctx = calloc(1, sizeof(struct gss_jose_ctx));
        if (jctx == NULL) {
            *minor_status = ENOMEM;
            return GSS_S_FAILURE;
        }
        jctx->initiator = true;
    } else {
        if (!jctx->initiator) {
            *minor_status = EINVAL;
            return GSS_S_FAILURE;
        }
    }

    *context_handle = (gss_ctx_id_t)jctx;

    return GSS_S_CONTINUE_NEEDED;
}

static OM_uint32 gss_jose_null_key(OM_uint32 *minor_status,
                                  const struct gss_jose_ctx *jctx,
                                  gss_buffer_set_t *data_set)
{
    uint8_t key_data[32] = {0};
    gss_buffer_desc key = {
        .length =  sizeof(key_data),
        .value = key_data,
    };
    uint8_t type_data[4] = {0};
    gss_buffer_desc type = {
        .length =  sizeof(type_data),
        .value = type_data,
    };
    OM_uint32 major;
    uint32_t enc_type = 0x0000; /* ENCTYPE_NULL */

    type_data[3] = (enc_type >> 24) & 0xff;
    type_data[2] = (enc_type >> 16) & 0xff;
    type_data[1] = (enc_type >>  8) & 0xff;
    type_data[0] = (enc_type      ) & 0xff;

    major = gss_add_buffer_set_member(minor_status, &key, data_set);
    if (major != GSS_S_COMPLETE) {
        return major;
    }
    major = gss_add_buffer_set_member(minor_status, &type, data_set);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    return GSS_S_UNAVAILABLE;
}

OM_uint32
gss_jose_inquire_sec_context_by_oid(OM_uint32 *minor_status,
                                    const gss_ctx_id_t context_handle,
                                    const gss_OID desired_object,
                                    gss_buffer_set_t *data_set)
{
    const struct gss_jose_ctx *jctx = (struct gss_jose_ctx *)context_handle;
    bool get_key = gss_oid_equal(desired_object, GSS_C_INQ_NEGOEX_KEY);
    bool get_verify_key = gss_oid_equal(desired_object, GSS_C_INQ_NEGOEX_VERIFY_KEY);

    if (jctx == NULL) {
        return GSS_S_UNAVAILABLE;
    }

    if (!get_key && !get_verify_key) {
        return GSS_S_UNAVAILABLE;
    }

    /* TODO FIXME Create a real key after metadata exchange! */
    return gss_jose_null_key(minor_status, jctx, data_set);
}
